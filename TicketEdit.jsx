import React, { Component } from 'react';
import Ticket from "./Ticket";
import Swal from 'sweetalert2';
import moment from 'moment';
import Toaster from '../../actions/Toaster';
import { getticketById, updateTicket } from "../TicketingSystem/ticketCRUD";

class TicketEdit extends Component {
     constructor(props) {
        super(props);
        this.state = {
            facility:'',
          date: moment(new Date()).toDate(),
          assignedTo: '',
          status:'',
          issue:'',
          loading: false,
          facilities:[],
          ticketData:[],
            }
         this.facilityChange = this.facilityChange.bind(this)
        this.outageDate = this.outageDate.bind(this)
        this.assigneeChange = this.assigneeChange.bind(this)
        this.statusChange = this.statusChange.bind(this)
        this.issueChange = this.issueChange.bind(this)
            this.handleSubmit = this.handleSubmit.bind(this)
           
        }
       

    componentDidMount() {
        let path = window.location.pathname.split("/")
        let ticketId = path[path.length-1]; 

        this.setState({
            ticketId:ticketId 
        },()=>{
            getTicketByID(this.state.ticketId)
            .then(res => {
                this.setState({
                facility:res.data.facilities,
                date: res.data.date,
                assignedTo: res.data.assignedTo,
                status: res.data.status,
                issue: res.data.issue,
                loading: false
                })
            })
            .catch(err => {
                this.catchError(err)
            })
        })
    }
    handleErrorSubmit = (e, formData, errorInputs) => {
        this.checkType();
    };
    checkType() {
        if (!this.state.selectedTypes.length) {
            this.setState(() => ({ 
                error: {
                    selectedTypes: "Please select a Type"
                }
            }) );
        }else{
            this.setState(() => ({ 
                error: {
                    selectedTypes: ""
                }
            }) );
        }
    }
    handleChange = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    };
    handleTicketType = selectedTypes => {
        this.setState({ selectedTypes:selectedTypes.map(type => type.value) });
    }
    handleSubmit(e){
        e.preventDefault();
        this.setState({ btnLoading: true})
        let formData = Object.fromEntries(new FormData(e.target))
       
        const ticket = {
            facility: formData.facility,
            date: formData.date,
            assignedTo: formData.assignedTo,
            status: formData.status,
            issue : formData.issue,
         
        }
        updateTicket(this.state.ticketId, ticket)
            .then(res => {
                if(res.status == 200){
                    this.setState({ btnLoading: false})
                    Swal.fire({
                        position: 'center',
                        type: 'success',
                        title: 'Ticket Updated',
                        showConfirmButton: false,
                        timer: 1800
                    })
                        .then(() => {
                            this.props.history.push('/portfolio/tickets')
                        });
                }
        }).catch(error => {
            this.catchError(error)
        })
    }

    catchError(err) {
        console.log(err);
        if (err.response) Toaster({ title: err.response.statusText }, 'error');
        this.setState(() => ({ catchError: true, loading: false, btnLoading: false }));
    }

    render() {
        const {tickets} = this.state
        if(!tickets){
            return(
                <div className="d-flex justify-content-center">
                    <div className="spinner-border" role="status">
                        <span className="sr-only">Loading...</span>
                    </div>
                </div>
              )
        }
        return (
            <Ticket 
                elementSubmit={this.handleSubmit}
                handleErrorSubmit={this.handleErrorSubmit}
                handleTicketType={this.handleOrganizationType}
                handleChange={this.handleChange}
                error={this.state.error}
                loading={this.state.loading}
                facility={this.state.facility}
                date={this.state.date}
                assignedTo={this.state.assignedTo}
                status={this.state.status}
                issue={this.state.issue}
                btnLoading={this.state.btnLoading}
                subModule='Edit'
                submitText="Update"
            />
        );
    }
}

export default TicketEdit;